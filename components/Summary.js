import { useEffect, useState, useRef} from 'react'
import { Row, Col, Card, Table } from 'react-bootstrap'

export default function Summary({data}){

    const [balance, setBalance] = useState([])
    const [dataLabel, setDataLabel] = useState([])
    const [datasetsData, setDatasetsData] = useState([])

    const [flip, setFlip] = useState(false)
    const [backSummary, setBackSummary] = useState()
    const frontEl = useRef()
    const backEl = useRef()
    
    useEffect(() => {

        const getLabel = data.map(data => data.transactionType)
        const dummy = getLabel.splice(0, getLabel.length, ...(new Set(getLabel)))    
        
        let num
        let array = []
        for(num=0;num<getLabel.length;num++){
            let total=0
            data.map(data => {
                if(data.transactionType==getLabel[num]){
                    total+=data.amount
                }
            })
            array.push(total)
        }

        let arrayTotal = 0
        array.forEach(element => arrayTotal+=element)
        const arrayPercent = array.map(element => {
            return Number(element/arrayTotal).toLocaleString(undefined,{style: 'percent', minimumFractionDigits:2}); 
        })

        setDataLabel(getLabel)
        setDatasetsData(array)

    }, [data])

    useEffect(() => {

        let total = 0
        const dummy = datasetsData.forEach(element => total+=element)
        let value = new Intl.NumberFormat('fullwide', { style: 'currency', currency: 'PHP' }).format(total)
        setBalance(value)

    }, [datasetsData])

    // useEffect(() => {

    //     let num
    //     let array = []
    //     for(num=0;num<dataLabel.length;num++){
   
    //             Object.assign(array, {`${dataLabel[num]}: ${datasetsData[num]}`})

    
    //     }
        
    //     console.log(array)
    //     // setBackSummary(value)

    // }, [datasetsData])

    console.log(`${dataLabel[0]}`)

    return (
        <React.Fragment>
            <Row className="justify-content-center mt-5">
                <h4>Summary</h4>
                <Card style={{ width: '18rem', height: '12rem' }}
                    className={`cardSummary ${flip ? 'flip' : ''}`} 
                    onClick={() => setFlip(!flip)}
                >
                    <Card.Body>
                        <div className="front" ref={frontEl}>
                            <Card.Title>{balance}</Card.Title>
                            <Card.Subtitle className="mb-2 text-muted">Remaining Budget</Card.Subtitle>
                            <Card.Text>
                                Click to see more details
                            </Card.Text>
                            <Card.Link href="/history">See all</Card.Link>
                            <Card.Link href="/transactions">Add</Card.Link>
                        </div>
                        <div className="back" ref={backEl}>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Transaction</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{dataLabel[0]}</td>
                                        <td>{datasetsData[0]}</td>
                                    </tr>
                                    <tr>
                                        <td>{dataLabel[1]}</td>
                                        <td>{datasetsData[1]}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Card.Body>
                </Card>
            </Row>
        </React.Fragment>
        
    )
}

