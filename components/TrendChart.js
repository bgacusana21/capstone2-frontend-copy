import { Bar } from 'react-chartjs-2'
import { useEffect, useState} from 'react'
import moment from 'moment'

export default function BarChart({data}){

    const [datasetsData, setDatasetsData] = useState([])
    
    useEffect(() => {

        let valueArray = [0,0,0,0,0,0,0,0,0,0,0,0]
        data.forEach(element => {
            const index = moment(element.transactionTimestamp).month()
            valueArray[index] += parseInt(element.amount)

        })
        setDatasetsData(valueArray)

    }, [data])

    return(
        <Bar data={
            {
                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                datasets: [
                    {
                        label: 'Remaning Balance',
                        backgroundColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: datasetsData
                    }
                ]
            }
        }redraw = { false } 
        />
    )

}

