import { useContext, useState, useEffect } from 'react'
import { Form, Button, Row, Col, Modal, Dropdown, DropdownButton } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function NavBar({data, openModal}) {

    const [showForm, setShowForm] = useState(false)
    const [isDebit, setIsDebit] = useState('')
    const [categoryName, setCategoryName] = useState('')
    const [value, setValue] = useState('')
    const [modalName, setModalName] = useState('')
    const [consoleAddCat, setConsoleAddCat] = useState('')

    const handleClose = () => {
        setIsDebit('')
        setCategoryName('')
        setValue('')
        setModalName('')
        setShowForm(false)
    }


    function addCategory(e) { 
        e.preventDefault()


        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/transactions/${data.categoryType}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                categoryType: data.categoryType,
                name: categoryName,
                value: value,
                mainCatValue: data.mainCatValue,
                isDebit: isDebit
            })
        })
        .then(res => res.json())
        .then(dataAdd => {
            setConsoleAddCat(dataAdd)
            if (dataAdd) {
                handleClose()
            } else {
                Swal.fire(
                    `Can't add category.`,
                    'Category check category name if under inactive categories .'
                )
            }
        })

    }

    useEffect(() => {
        setModalName(data.name)
    }, [])    

    useEffect(() => {
        setShowForm(openModal)
        setModalName(data.name)
    }, [data])

    useEffect(() => {
        setValue(categoryName.trim().toLowerCase().replace(/\s/g, ''))
    }, [categoryName])

    return (
        <React.Fragment>
            <Modal show={showForm} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add {data.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => addCategory(e)}>
                        <Form.Group controlId="mainCategoryName">
                            <Form.Label>Category Name:</Form.Label>
                            <Form.Control 
                                type="text"
                                placeholder="Enter main category name"
                                onChange={e => {
                                    setCategoryName(e.target.value)
                                    setModalName(data.name)
                                }}
                                required
                            />
                        </Form.Group>
                        {(data.categoryType==='mainCategory')
                        ?
                        <Form.Group controlId="transactionType">
                            <Form.Label>Transaction Type</Form.Label>
                            <select className="form-control" name="transactionType" onChange={e => setIsDebit(e.target.value)}>
                                    <option selected>Please select 1</option>
                                    <option value="true">Debit</option>
                                    <option value="false">Credit</option>
                            </select>
                        </Form.Group>
                        :
                        null
                        }

                        <Modal.Footer>
                            <Button className="bg-primary" type="submit">
                                Submit
                            </Button>
                            <Button className="bg-secondary" onClick={handleClose}>
                                Close
                            </Button>
                        </Modal.Footer>
                    </Form>
                </Modal.Body>
            </Modal>
        </React.Fragment>
    )
}