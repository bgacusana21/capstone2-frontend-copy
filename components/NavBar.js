import { Navbar, Nav, NavDropdown, Form, FormControl, Button } from 'react-bootstrap'
import { useContext } from 'react'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {

    const { user } = useContext(UserContext)

    return (
        <Navbar id="navBar" className="bg-light mb-2" expand="lg" sticky="top" >
            <Navbar.Brand href="/">
                <img
                alt=""
                src="/BTALogo.png"
                width="30"
                height="30"
                className="d-inline-block align-top"
                />{' '}
                Budget Tracking
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav" >
                <Nav >
                    { user.email === null
                    ?
                    <React.Fragment>
                        <NavDropdown title="Use Budget Tracking" id="basic-nav-dropdown" href="/login">
                            <Nav.Link className="navMenu" href="/login">Login</Nav.Link>
                            <Nav.Link className="navMenu" href="/register">Create an Accont</Nav.Link>
                        </NavDropdown> 
                        <Nav.Link className="navMenu" href="/faq">FAQ</Nav.Link>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <Nav.Link href="/">Dashboard</Nav.Link>
                        <NavDropdown title="Transactions" id="basic-nav-dropdown" href="/transactions">
                            <Nav.Link className="navMenu" href="/transactions">Add Transactions</Nav.Link>
                            <Nav.Link className="navMenu" href="/trends">Trends and Analysis</Nav.Link>
                            <Nav.Link className="navMenu" href="/history">History</Nav.Link>
                        </NavDropdown>
                        <NavDropdown title="Account" id="basic-nav-dropdown" href="/account">
                            <Nav.Link className="navMenu" href="/account">Account</Nav.Link>
                            <Nav.Link className="navMenu" href="/addCategory">Categories</Nav.Link>
                            <Nav.Link className="navMenu" href="/logout">Logout</Nav.Link>
                        </NavDropdown>
                        <Nav.Link className="navMenu" href="/faq">FAQ</Nav.Link>
                    </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}


