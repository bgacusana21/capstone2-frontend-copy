import { useContext, useEffect, useState, useRef } from 'react'
import { Row, Col, Button, Tabs, Tab, Form, Container } from 'react-bootstrap'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import Swal from 'sweetalert2'

import UserContext from '../UserContext'
import Doughnut from '../components/Doughnut'
import TrendChart from '../components/TrendChart'
import Summary from '../components/Summary'

export default function index() {
    const { userInfo, userTransactions } = useContext(UserContext)
    const [year, setYear] = useState('')
    const [yearSelection, setYearSelection] = useState([])
    const [month, setMonth] = useState('')
    const [monthSelection, setMonthSelection] = useState([])
    const [filteredData, setFilteredData] = useState([])
    const removeFilter = useRef(0)
    
    const [startDate, setStartDate] = useState(null)
    const [endDate, setEndDate] = useState(null)

    const [test, setTest] = useState([])

    function applyFilter(){

        
        if (startDate>endDate || endDate>new Date() || startDate==null || endDate==null){      
            console.log('inside 1')    
            setStartDate(null)
            setEndDate(null)
            Swal.fire(
                'Cannot filter selected dates!',
                `Please check start date`,
            )
        } else {
            console.log('inside 2')    
            const filtered = userTransactions.filter(element => (moment(element.transactionTimestamp)>=moment(startDate)&&moment(element.transactionTimestamp)<=moment(endDate).add(1, 'days')))
            setFilteredData(filtered)

        }
    }

    useEffect(() => {
        console.log(filteredData)
    },[filteredData])

    function resetFilter(){
        setYear('')
        setYearSelection([])
        setMonth('')
        setMonthSelection([])
        setStartDate(null)
        setEndDate(null)
        removeFilter.current =  removeFilter.current + 1
    }

    useEffect(() => {
        const mapped = userTransactions.map(data => moment(data.transactionTimestamp).format('YYYY') )
        const year = mapped.splice(0, mapped.length, ...(new Set(mapped)))
        const mappedSelect = mapped.map(data => {
            return <option key={data} value={data}>{data}</option>
        })
        setYearSelection(mappedSelect)
        setFilteredData(userTransactions)
    },[userTransactions, removeFilter.current])

    useEffect(() => {
        setMonth('')
        const filteredYear = userTransactions.filter(data => {
            return moment(data.transactionTimestamp).format('YYYY')===year
        })
        const mapped = filteredYear.map(data => moment(data.transactionTimestamp).format('MMM') )
        const month = mapped.splice(0, mapped.length, ...(new Set(mapped))).sort((a,b) => b - a)
        const mappedSelect = mapped.map(data => {
            return <option key={data} value={data}>{data}</option>
        })
        setMonthSelection(mappedSelect)        
    }, [year])

    useEffect(() => {
        setFilteredData(userTransactions)
        if((month=='')&&(year!='')){
            const filteredYear = userTransactions.filter(data => {
                return moment(data.transactionTimestamp).format('YYYY')===year
            })
            setFilteredData(filteredYear)
        } else if ((month!='')&&(year!='')){
            const filteredYear = userTransactions.filter(data => {
                return moment(data.transactionTimestamp).format('YYYY')===year
            })
            const filteredMonth = filteredYear.filter(data => {
                return moment(data.transactionTimestamp).format('MMM')===month
            })   
            setFilteredData(filteredMonth)
        } 
    }, [year, month])

	return (
        <React.Fragment>
            <div className="px-3">
                <h1>Trends and Analysis</h1>
                <Row>
                    <Col xs={11} md={4}>
                        <Tabs  defaultActiveKey="populated" id="trendFilter">
                            <Tab eventKey="populated" title="Filter Date">
                                <Container>
                                    <div className="trend-date-form">
                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label>Year</label>
                                                <select className="form-control" name="year" onChange={e => {
                                                    setYear(e.target.value)
                                                    }}>
                                                        <option value='' selected>Select 1</option>
                                                        {yearSelection}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="form-group col-md-6">
                                                <label>Month</label>
                                                <select className="form-control" name="month" onChange={e => {
                                                    setMonth(e.target.value)
                                                    }}>
                                                        <option value='' selected>Select 1</option>
                                                        {monthSelection}
                                                </select>
                                            </div>
                                        </div>
                                        <Row className="px-3">
                                            <Button className="bg-secondary mr-2" onClick={e => resetFilter()}>Reset Filter</Button>
                                        </Row>
                                    </div>
                                </Container>
                            </Tab>
                            <Tab eventKey="customeDate" title="Custom Date">
                                <Container>
                                    <Row  className="mt-3">
                                        <Col xs={4} md={4}>
                                            <Form.Label>Start Date</Form.Label>
                                        </Col>
                                        <Col xs={8} md={8}>
                                            <DatePicker
                                                selected={startDate}
                                                placeholderText="Select start date"
                                                onChange={date => setStartDate(date)}
                                            >
                                            <div style={{ color: "red" }}>Start date must be before end date!</div>
                                            </DatePicker>    
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={4} md={4}>
                                            <Form.Label>End Date</Form.Label>
                                        </Col>
                                        <Col xs={8} md={8}>
                                            <DatePicker
                                                selected={endDate}
                                                placeholderText="Select end date"
                                                onChange={date => setEndDate(date)}
                                            />
                                        </Col>
                                    </Row>
                                    <Row className="mt-2">
                                        <Col>
                                            <Button className="bg-secondary mr-2" onClick={e => applyFilter()}>Apply Filter</Button>
                                            <Button className="bg-secondary mr-2" onClick={e => resetFilter()}>Reset Filter</Button>
                                        </Col>
                                    </Row>
                                </Container>
                            </Tab>
                        </Tabs>
                    
                        <Summary data={filteredData}/>
                    </Col>
                    <Col xs={11} md={8}>
                        
                        <Doughnut data={filteredData} />
                        <TrendChart data={filteredData}/>
                    </Col>
                </Row>
            </div>
        </React.Fragment>
        
    )
}

