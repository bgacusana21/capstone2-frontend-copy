import { useRef, useState, useEffect } from 'react'
import { Form, FormControl, Button, Accordion, Row, Col, Card } from 'react-bootstrap'
import DatePicker from 'react-datepicker'

import TransactionHistory from '../components/TransactionsHistory'

export default function history() {

    const [mainCatRaw, setMainCatRaw] = useState([])
    const [mainCatSelection, setMainCatSelection] = useState([])
    const [subCatSelection, setSubCatSelection] = useState([])

    const [mainCat, setMainCat] = useState('')
    const [subCat, setSubCat] = useState('')
    const [keyword, setKeyword] = useState('')
    const dateRaw = new Date()
    const [startDate, setStartDate] = useState(dateRaw.setMonth(dateRaw.getMonth()-1))
    const [endDate, setEndDate] = useState(new Date())
    const [searchData, setSearchData] = useState([])
    const removeFilter = useRef(0)

    const [open, setOpen] = useState(false)
    const [filterBtn, setFilterBtn] = useState('')

    useEffect(()=>{
        {open
            ?
            setFilterBtn('Close Filter')
            :
            setFilterBtn('Open Filter')
        }
    },[open])
    
    function applyFilter(){
        
        if(startDate>endDate){
            return console.log('start date must be before end date')
        } else{
            setSearchData([{
                mainCat: mainCat,
                subCat: subCat,
                keyword: keyword,
                startDate: startDate,
                endDate: endDate
            }])
        }
    }

    function resetFilter(){
        setMainCat('')
        setKeyword('')
        setStartDate(dateRaw.setMonth(dateRaw.getMonth()-1))
        setEndDate(new Date())
        applyFilter()
        setSearchData([])
        setMainCatSelection([])
        removeFilter.current += 1

    }
    
    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/transactions/mainCategory`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setMainCatRaw(data)           
        })
    }, [])

    useEffect(() => {
        const mapped = mainCatRaw.map(data => {
                return (
                    <option key={data.id} value={data.value}>{data.name}</option>
                )
        })
        setMainCatSelection(mapped)
    },[mainCatRaw, searchData, removeFilter])

    useEffect(() => {
    
    if(mainCat!==''){
        if(mainCatRaw.length>0){
            const subCatSelected = mainCatRaw.filter(data => {
                if(data.value===mainCat){
                    return data
                }
            })
    
            const mapped = subCatSelected[0].categorySelection.map(data => {
                return (
                    <option key={data.id} value={data.value}>{data.name}</option>
                )  
            })
            setSubCatSelection(mapped)
        }
    } else setSubCatSelection('')
    },[mainCat])

    return (
        <React.Fragment>
            <h1>Transactions</h1>
            <Row>
                <Col xs={11} md={9}>
                    <Accordion>
                        <Card>
                            <Card.Header>
                                <Accordion.Toggle as={Button} eventKey="0" onClick={()=>setOpen(!open)}>
                                {filterBtn}
                                </Accordion.Toggle>
                            </Card.Header>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Row>
                                        <Col>
                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>Transaction Type</label>
                                                    <select className="form-control" name="transactionType" onChange={e => {
                                                        setMainCat(e.target.value)
                                                        }}>
                                                            <option value='' selected>Select 1</option>
                                                            {mainCatSelection}
                                                    </select>
                                                </div>
                                            </div>

                                            <div className="form-row">
                                                <div className="form-group col-md-6">
                                                    <label>Sub Category Type</label>
                                                    <select className="form-control" name="transactionType" onChange={e => {
                                                        setSubCat(e.target.value)
                                                        }}>
                                                            <option value='' selected>Select 1</option>
                                                            {subCatSelection}
                                                    </select>
                                                </div>
                                            </div>

                                            <Form.Group controlId="formBasicEmail">
                                                <Form.Label>Keyword</Form.Label>
                                                <FormControl type="text" value={keyword} onChange={e => setKeyword(e.target.value)} placeholder="Search" className="mr-sm-2" />
                                            </Form.Group>

                                        </Col>
                                            
                                        <Col>
                                            <Form.Group controlId="date">
                                                <Row>
                                                    <Col>
                                                        <Form.Label>Start Date</Form.Label>
                                                        <DatePicker
                                                            selected={startDate}
                                                            placeholderText="Select start date"
                                                            onChange={date => setStartDate(date)}
                                                        >
                                                        <div style={{ color: "red" }}>Start date must be before end date!</div>
                                                        </DatePicker>    
                                                    </Col>
                                                    <Col>
                                                        <Form.Label>End Date</Form.Label>
                                                        <DatePicker
                                                            selected={endDate}
                                                            placeholderText="Select end date"
                                                            onChange={date => setEndDate(date)}
                                                        />
                                                    </Col>
                                                </Row>

                                            </Form.Group>

                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Button className="bg-secondary mr-2" onClick={e => applyFilter()}>Apply Filter</Button>
                                        <Button className="bg-secondary mr-2" onClick={e => resetFilter()}>Reset Filter</Button>
                                    </Row>

                                </Card.Body>
                                
                            </Accordion.Collapse>
                        </Card>


                    </Accordion>
                    
                </Col>
            </Row>

            <Row className="mt-3">
                <Col xs={11} md={9}>
                    <TransactionHistory length='1000' searchData={searchData}/>
                </Col>
            </Row>  
        </React.Fragment>
        
    )
}
